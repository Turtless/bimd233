var lis = document.querySelectorAll("li");

lis.forEach(function (elem) {
  elem.addEventListener("mouseover", function () {
    elem.innerHTML = "selected"
    console.log("mouseover");
  });

  elem.addEventListener("mouseout", function () {
    elem.innerHTML = "deselected"
    console.log("mouseout");
  });

  elem.addEventListener("click", function () {
    elem.innerHTML = "Focused!"
    console.log("clicked");
  });
});