var scores = [];

// scores.push({
//   school: "",
//   conference: "",
//   overall: "",
//   last_game: ""
// });

scores.push({
  school: "OREGON",
  conference: "6-2",
  overall: "10-2",
  last_game: "W 38-29",
  letters: "ou"
});

scores.push({
  school: "WASHINGTON STATE",
  conference: "6-3",
  overall: "7-5",
  last_game: "W 40-13",
  letters: "wsu"
});

scores.push({
  school: "OREGON STATE",
  conference: "5-4",
  overall: "7-5",
  last_game: "W 29-38",
  letters: "osu"
});

scores.push({
  school: "CALIFORNIA",
  conference: "3-5",
  overall: "4-7",
  last_game: "W 14-42",
  letters: "cal"
});

scores.push({
  school: "WASHINGTON",
  conference: "3-6",
  overall: "4-8",
  last_game: "L 13-40",
  letters: "uw"
});

scores.push({
  school: "STANFORD",
  conference: "2-7",
  overall: "3-9",
  last_game: "L 14-45",
  letters: "su"
});

function mainFunction() {
  var nodeList = document.querySelectorAll("li.north-div");
  // console.log("Length Data:", scores.length, nodeList.length);
  nodeList.forEach(populateRow);
}

function populateRow(obj, i) {
  console.log("Obj:", obj, i);
  obj.innerHTML = buildRowHTML(scores[i]);
  obj.id = scores[i].letters;
}

function buildRowHTML(data) {
  let strHTML = '<div class="row">';
  strHTML += '<div class="col-sm college">' + data.school + "</div>";
  strHTML += '<div class="col-sm conference">' +data.conference + "</div>";
  strHTML += '<div class="col-sm overall">' + data.overall + "</div>";
  strHTML += '<div class="col-sm lastgame">' + data.last_game + "</div>";
  return strHTML;
}

//    ASU @  OREGON11/27ESPN CLICK TO SHOW MORE INFORMATION
// WASHINGTON STATE  WASHINGTON STATE 5-3 6-5 W 44-18 ARIZ  @  WASHINGTON11/26FS1 CLICK TO SHOW MORE INFORMATION
// CALIFORNIA  CALIFORNIA 3-4 4-6 W 41-11 STAN  @  UCLA11/27FS1 CLICK TO SHOW MORE INFORMATION
// WASHINGTON  WASHINGTON 3-5 4-7 L 17-20 COL VS  WASHINGTON STATE11/26FS1 CLICK TO SHOW MORE INFORMATION
// STANFORD  STANFORD 2-7 3-8 L 11-41 CAL