$(document).ready(function() {
    // add a 5px red dashed boarder around the panel div
    // provide a 10 pixel padding around all of the divs
    $('.card').css('border', '5px dashed red');
    $('.card').css('padding', '10px');

    // set all div's padding to 3px
$('.card-body').css('padding', '3px');
    // set all divs in the panel to background gray
  $('.card-body').css('background-color', 'gray');

    // set all divs of class cat to green
  $('.cat').css('color', 'green');
 $(".cat, #calico").last().css("color", "white");
    // set all divs of class dog to red
$('.dog').css('color', 'red');
    // set the id of lab to a 1px solid yellow border
$('#lab').css('border', '1px solid yellow');
    // set the last div with class dog to background yellow
$('.dog').css('background', 'yellow');
    // set the calico cat's width to 50%, 
 $('.cat, #calico').last().css('width', '50%');
    // background to green and color to white 
  $('.cat, #calico').last().css('background-color', 'green');
  });