var header_data = [
  "Flight #",
  "Arrival Airport",
  "Arrived",
  "Baggage Area"
];
var flight_data = [
  {
    "Flight #": "ASA1077",
    "Aircraft Type": "A319",
    "Departure Airport": "Washington Dulles Intl (KIAD)",
    "Arrival Airport": "San Francisco Intl (KSFO)",
    Departed: "Wed 07:32PM EST",
    Arrived: "Wed 10:10PM PST",
    Baggage_Area: "A1",
    
  },
  {
    "Flight #": "ASA1088",
    "Aircraft Type": "A320",
    "Departure Airport": "San Francisco Intl (KSFO)",
    "Arrival Airport": "Washington Dulles Intl (KIAD)",
    Departed: "Wed 03:58PM PST",
    Arrived: "Wed 11:28PM EST",
    Baggage_Area: "A2",
  },
  {
    "Flight #": "ASA1097",
    "Aircraft Type": "A320",
    "Departure Airport": "Washington Dulles Intl (KIAD)",
    "Arrival Airport": "Los Angeles Intl (KLAX)",
    Departed: "Wed 05:06PM EST",
    Arrived: "Wed 07:24PM PST",
    Baggage_Area: "A3",
  },
  {
    "Flight #": "ASA11",
    "Aircraft Type": "B739",
    "Departure Airport": "Newark Liberty Intl (KEWR)",
    "Arrival Airport": "Seattle-Tacoma Intl (KSEA)",
    Departed: "Wed 05:00PM EST",
    Arrived: "Wed 07:27PM PST",
    Baggage_Area: "A4",
  },
  {
    "Flight #": "ASA1113",
    "Aircraft Type": "A320",
    "Departure Airport": "Will Rogers World (KOKC)",
    "Arrival Airport": "Seattle-Tacoma Intl (KSEA)",
    Departed: "Wed 05:40PM CST",
    Arrived: "Wed 07:11PM PST",
    Baggage_Area: "A5",
  }
];

function DateDiff(D1, D2) {
  var t2 = new Date(D1);
  var t3 = new Date(D2);

  var diff = t3.getTime() - t2.getTime();  //ms
  
  return diff;
}

//DeltaT is in millisecondss
function FormatTimeHHMMSS(dT) {
  
  let hr = parseInt(dT / 1000 / 3600);
  let min = parseInt((dT - hr * 3600 * 1000) / 1000 / 60);
  let sec = parseInt((dT - hr * 3600 * 1000 - min * 60 * 1000) / 1000);
 
  //TODO To convert  the three above integers  into HH:MM:SS
  let strTime = "HH:MM:dSS";
  
  return [hr+":"+min+":"+sec];
}

function build_html_header(array_data) {
  let html_str = '<div class="row ">';
  for (var i = 0; i < array_data.length; i++) {
    html_str += '<div class="col heading">' + array_data[i] + "</div>";
  }
  html_str += "</div>";
  return html_str;
}

function build_html_flight(data) {
  let html_str = "";
  for (var i = 0; i < data.length; i++) {
    html_str += '<div class="row">';
    html_str += '<div class="col">' + data[i]["Flight #"] + "</div>";
    html_str += '<div class="col">' + data[i]["Arrival Airport"] + "</div>";
    html_str += '<div class="col">' + data[i]["Arrived"] + "</div>";
    html_str += '<div class="col">' + data[i]["Baggage_Area"] + "</div>";
    html_str += "</div>";
  }
  html_str += "</div>";
  return html_str;
}

for (var i = 0; i < flight_data.length; i++) {
  console.log(flight_data[i]);
}

var el = document.getElementById("flight-data");
el.innerHTML = build_html_header(header_data);
el.innerHTML += build_html_flight(flight_data);

//This is test
d2 = new Date("2021-11-17 19:27:11");
d1 = new Date("2021-11-17 14:00:01");
var ans = FormatTimeHHMMSS(d2-d1);