var myHigh = [82, 75, 69, 69, 68];
var myLow = [55, 52,52,48,51];
var msg = "";

var header_data = [
  "Days",
  "High",
  "Low",
];

var wx_data = [
  {
    day: "fri",
    hi: 82,
    lo: 55
  },
  {
    day: "sat",
    hi: 75,
    lo: 52
  },
  {
    day: "sun",
    hi: 69,
    lo: 52
  },
  {
    day: "mon",
    hi: 69,
    lo: 48
  },
  {
    day: "tue",
    hi: 68,
    lo: 51
  }
];

function build_html_header(array_data) {
  let html_str = '<div class="row ">';
  for (var i = 0; i < array_data.length; i++) {
    html_str += '<div class="col heading">' + array_data[i] + "</div>";
  }
  html_str += "</div>";
  return html_str;
}

function build_html_weather(data) {
  let html_str = "";
  for (var i = 0; i < data.length; i++) {
    html_str += '<div class="row">';
    html_str += '<div class="col">' + data[i]["day"] + "</div>";
    html_str += '<div class="col">' + data[i]["hi"] + "</div>";
    html_str += '<div class="col">' + data[i]["lo"] + "</div>";
    html_str += "</div>";
    //console.log(wx_data[i]);
  }
  html_str += "</div>";
  return html_str;
}

for (var i = 0; i < wx_data.length; i++) {
  console.log(wx_data[i]);
}

function avgTemp(weatherData, daily) {
  const data = [];
  for (var i = 0; i < weatherData.length; i++) {
    data[i] = weatherData[i][daily];
  }
  var avg = data.reduce(getSum, 0) / data.length;
  return avg;
}

function getSum(total, num) {
  return total + Math.round(num);
}

function build_html_footer(data) {
  let html_str = '<div class="row">';
  html_str +=
    '<div class="col heading">' +
    "Average: " +
    avgTemp(data, "hi") +
    "/" +
    avgTemp(data, "lo") +
    "</div>";
  html_str += "</div>";
  return html_str;
}

/*function findAvg(wx_data) {
  var avgTemp = 0;
  var counts = wx_data.length;
  for (var i = 0; i < counts; i++) {
  high = wx_data[i].hi;
    avgTemp += findAvg(high);
  }
  return avgTemp/ counts;
}
*/

for (var i = 0; i < wx_data.length; i++) {
  high = wx_data[i].hi;
  low = wx_data[i].lo;
  // console.log("hello!");
}

var el = document.getElementById("wx-data");
el.innerHTML += build_html_header(header_data);
el.innerHTML += build_html_weather(wx_data);
el.innerHTML += build_html_footer(wx_data);