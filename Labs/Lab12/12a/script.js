var radii = [];
const NUM_RADII = 3;

function calcCircleGeometries(radius) {
  const pi = Math.PI;
  var area = pi * radius * radius;
  var circumference = 2 * pi * radius;
  var diameter = 2 * radius;
  var geometries = [area, circumference, diameter];

  return geometries;
}
function printAllRows(data) {
  const N = data.length;
  for (var i = 0; i < N; i++) {
    let r = data[i];
    //console.log(r, calcCircleGeometries(r));
    let el = document.getElementById("data");
    //el.innerHTML = '<div class="row"';

    renderRow(r, calcCircleGeometries(r));
  }
}

function renderRow(r, geoms) {
  let el = document.getElementById("data");
  el.innerHTML += "<div class='rol'>";
  el.innerHTML += "<div class='col'>" + r + "</div>";
  el.innerHTML += "<div class='col'>" + geoms[0].toFixed(2) + "</div>";
  el.innerHTML += "<div class='col'>" + geoms[1].toFixed(2) + " </div>";
  el.innerHTML += "<div class='col'>" + geoms[2].toFixed(1) + " </div>";
  el.innerHTML += "</div>";
}

for (var i = 0; i < NUM_RADII; i++) {
  radii[i] = Math.random() * 100;
  let r = radii[i];
  console.log(r, calcCircleGeometries(r));
}
printAllRows(radii);